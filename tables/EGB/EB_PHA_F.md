# EB_PHA_F

Table des données de codage de pharmacie


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|PHA_PRS_C13|nombre réel|Code CIP de la pharmacie de ville (13 Caractères)|||
|PHA_ORD_NUM|nombre réel|Numéro d'ordre de la prestation affinée pharmacie|||
|PHA_ACT_QSN|nombre réel|Quantité affinée signée (= nombre de boites facturées)|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|PHA_MOD_PRN|chaîne de caractères|Mode de prescription|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|PHA_CPA_PCP|chaîne de caractères|Condition particulière de prise en charge|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|PHA_IDE_CPL|nombre réel|Préfixe du code CIP|||
|PHA_PRS_IDE|nombre réel|Code CIP de la pharmacie de ville (ancien code sur 7 Caractères)|||
|PHA_SEQ_RNV|nombre réel|Séquence de renouvellement|||
|PHA_ACT_PRU|nombre réel|Prix unitaire du médicament codé en CIP|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|PHA_DEC_TOP|chaîne de caractères|Top déconditionnement|||
|PHA_DEC_QSU|nombre réel|Quantité complète de déconditionnement signée|||
|PHA_SUB_MTF|nombre réel|Motif de substitution du médicament|||
|PHA_DEC_PRU|nombre réel|Prix unitaire de l'unité déconditionnée délivrée|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
