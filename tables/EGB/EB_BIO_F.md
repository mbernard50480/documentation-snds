# EB_BIO_F

Table des données de codage de biologie


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|BIO_PRS_IDE|nombre réel|Code NABM de l'acte de biologie|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|BIO_ACT_QSN|nombre réel|Quantité d'actes de biologie|||
|BIO_ORD_NUM|nombre réel|Numéro d'ordre de l'acte affiné de biologie|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
